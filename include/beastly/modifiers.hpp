#pragma once

#include <boost/beast/http/field.hpp>
#include <string>

namespace beastly {

    class sbody {
    public:
        explicit sbody(const std::string& body)
            : _body(body) {}

        [[nodiscard]] const std::string& body() const { return _body; }

    private:
        const std::string& _body;
    };

    class header {
    public:
        header(boost::beast::http::field f, const std::string& value)
            : _field(f)
            , _value(value) {}

        [[nodiscard]] boost::beast::http::field field() const { return _field; }

        [[nodiscard]] const std::string& value() const { return _value; }

    private:
        boost::beast::http::field _field;
        const std::string&        _value;
    };

}  // namespace beastly
