#pragma once

#include <beastly/internal/handle_modifier.hpp>
#include <beastly/uri.hpp>
#include <boost/beast/http/field.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/verb.hpp>
#include <boost/beast/version.hpp>

namespace beastly::internal {

    boost::beast::http::response<boost::beast::http::string_body>
    perform_request(const beastly::uri& u, const boost::beast::http::request<boost::beast::http::string_body>& req);

    boost::beast::http::response<boost::beast::http::string_body>
    following_perform_request(const beastly::uri&                                           uri,
                              boost::beast::http::request<boost::beast::http::string_body>& req, uint8_t limit = 2);

    inline void handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& /*req*/) {
        // no-op
    }

    template <typename Mod>
    inline void handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& req, Mod&& cur) {
        handle_modifier(req, std::forward<Mod>(cur));
    }

    template <typename Mod, typename... Mods>
    inline void handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& req, Mod&& cur,
                                 Mods&&... next) {
        handle_modifier(req, std::forward<Mod>(cur));
        handle_modifiers(req, std::forward<Mods>(next)...);
    }

}  // namespace beastly::internal

namespace beastly {

    template <typename... Mods>
    boost::beast::http::response<boost::beast::http::string_body>
    perform_request(boost::beast::http::verb v, const beastly::uri& uri, Mods&&... mods) {
        boost::beast::http::request<boost::beast::http::string_body> req{v, uri.target(), 11};
        req.set(boost::beast::http::field::host, uri.host());
        req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);

        internal::handle_modifiers(req, std::forward<Mods>(mods)...);

        return internal::following_perform_request(uri, req);
    }

#define VERB_FUNCTION(name)                                                                                            \
    template <typename... Mods>                                                                                        \
    boost::beast::http::response<boost::beast::http::string_body> name(const uri& uri, Mods&&... ts) {                 \
        return perform_request(boost::beast::http::verb::name, uri, std::forward<Mods>(ts)...);                        \
    }

    VERB_FUNCTION(get)
    VERB_FUNCTION(post)
    VERB_FUNCTION(put)
    VERB_FUNCTION(patch)
    VERB_FUNCTION(delete_)

#undef VERB_FUNCTION

}  // namespace beastly
