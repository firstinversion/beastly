#pragma once

#include <beastly/internal/implementations/shared.hpp>
#include <beastly/uri.hpp>
#include <boost/asio/ip/basic_resolver.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/read.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/write.hpp>

namespace beastly::internal {

    template <typename Token>
    class async_perform_request_impl {
    public:
        async_perform_request_impl(beastly::uri uri, boost::beast::http::request<boost::beast::http::string_body> req,
                                   Token& token)
            : _memory(std::make_unique<memory>(std::move(uri), std::move(req), token)) {}

        template <typename Self>
        void operator()(Self& self) {
            assert(_memory->_state == memory::starting);
            _memory->_state = memory::resolving;
            _memory->_resolver.async_resolve(
                _memory->_uri.host(), use_port_with_default(_memory->_uri), std::move(self));
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec,
                        const boost::asio::ip::tcp::resolver::results_type& resolver_results) {
            assert(_memory->_state == memory::resolving);
            if (ec)
                self.complete(ec, {});
            else {
                _memory->_state = memory::connecting;
                _memory->_stream.async_connect(resolver_results, std::move(self));
            }
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec,
                        const boost::asio::ip::tcp::resolver::results_type::endpoint_type&) {
            assert(_memory->_state == memory::connecting);
            if (ec)
                self.complete(ec, {});
            else {
                _memory->_state = memory::writing;
                boost::beast::http::async_write(_memory->_stream, _memory->_request, std::move(self));
            }
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec, std::size_t bytes_transferred) {
            switch (_memory->_state) {
            case memory::writing:
                boost::ignore_unused(bytes_transferred);
                if (ec)
                    self.complete(ec, {});
                else {
                    _memory->_state = memory::reading;
                    boost::beast::http::async_read(
                        _memory->_stream, _memory->_read_buffer, _memory->_response, std::move(self));
                }
                break;
            case memory::reading:
                if (ec && ec != boost::beast::errc::not_connected && ec != boost::asio::error::eof &&
                    !is_ssl_short_read(ec))
                    self.complete(ec, {});
                else
                    self.complete({}, std::move(_memory->_response));
                break;
            default: assert(false); break;  // Should be unreachable.
            }
        }

    private:
        struct memory {
            enum { starting, resolving, connecting, writing, reading } _state;
            uri                                                           _uri;
            boost::beast::http::request<boost::beast::http::string_body>  _request;
            boost::asio::ip::tcp::resolver                                _resolver;
            boost::beast::tcp_stream                                      _stream;
            boost::beast::flat_buffer                                     _read_buffer;
            boost::beast::http::response<boost::beast::http::string_body> _response;

            memory(beastly::uri uri, boost::beast::http::request<boost::beast::http::string_body> req, Token& token)
                : _state(starting)
                , _uri(std::move(uri))
                , _request(std::move(req))
                , _resolver(boost::asio::get_associated_executor(token))
                , _stream(boost::asio::get_associated_executor(token)) {}
        };

        /**
         * Several components like Sockets really don't like to be moved, in spite
         * of technically supporting it.
         */
        std::unique_ptr<memory> _memory;
    };

}  // namespace beastly::internal
