#pragma once

#include <beastly/uri.hpp>
#include <boost/assert.hpp>
#include <boost/beast/core/error.hpp>

namespace beastly::internal {

    inline const char* use_port_with_default(const beastly::uri& uri) {
        if (!uri.port().empty()) return uri.port().data();
        switch (uri.protocol()) {
        case protocol::http: return "80";
        case protocol::https: return "443";
        }
    }

    inline bool is_ssl_short_read(const boost::beast::error_code& ec) {
        if (ec.value() != 335544539) return false;
        if (std::string_view{ec.category().name()} != "asio.ssl") return false;
        BOOST_ASSERT(ec.message() == "short read");
        return true;
    }

}  // namespace beastly::internal
