#define GET_VAL(clazz, type, name)                                                                                     \
    type name() const { return _##name; }                                                                              \
    type get_##name() const { return _##name; }

#define GET_REF(clazz, type, name)                                                                                     \
    const type& name() const { return _##name; }                                                                       \
    const type& get_##name() const { return _##name; }

#define GET_SET_VAL(clazz, type, name)                                                                                 \
    type   name() const { return _##name; }                                                                            \
    type   get_##name() const { return _##name; }                                                                      \
    clazz& name(type v) {                                                                                              \
        _##name = v;                                                                                                   \
        return *this;                                                                                                  \
    }                                                                                                                  \
    clazz& set_##name(type v) {                                                                                        \
        _##name = v;                                                                                                   \
        return *this;                                                                                                  \
    }

#define GET_SET_REF(clazz, type, name)                                                                                 \
    const type& name() const { return _##name; }                                                                       \
    const type& get_##name() const { return _##name; }                                                                 \
    clazz&      name(type v) {                                                                                         \
        _##name = std::move(v);                                                                                   \
        return *this;                                                                                             \
    }                                                                                                                  \
    clazz& set_##name(type v) {                                                                                        \
        _##name = std::move(v);                                                                                        \
        return *this;                                                                                                  \
    }

#define GET_SET_CONTAINER(clazz, type, name)                                                                           \
    const type& name() const { return _##name; }                                                                       \
    const type& get_##name() const { return _##name; }                                                                 \
    clazz&      name(type v) {                                                                                         \
        _##name = std::move(v);                                                                                   \
        return *this;                                                                                             \
    }                                                                                                                  \
    clazz& set_##name(type v) {                                                                                        \
        _##name = std::move(v);                                                                                        \
        return *this;                                                                                                  \
    }                                                                                                                  \
    type& name() { return _##name; }
