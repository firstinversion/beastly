#!/usr/bin/env python3
import os
import csv
import requests
from typing import Iterator


def drop(count: int, itr: Iterator) -> Iterator:
    for _ in range(0, count):
        next(itr)
    return itr


def download() -> str:
    chain_url = 'https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV'
    r = requests.get(chain_url)
    with open('./tmp.csv', 'w') as file:
        file.write(r.text)
    return './tmp.csv'


def transform(input_filepath: str) -> str:
    with open(input_filepath, 'r') as in_file:
        with open('./tmp.txt', 'w') as out_file:
            reader = csv.reader(in_file)
            for row in drop(1, reader):
                out_file.write(row[30].replace('\'', ''))
                out_file.write('\n')
    return './tmp.txt'


tmp_csv_path = download()
tmp_txt_path = transform(tmp_csv_path)

with open(tmp_txt_path, 'r') as in_file:
    for row in in_file:
        print(row, end='')

os.remove(tmp_csv_path)
os.remove(tmp_txt_path)
