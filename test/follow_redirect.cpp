#include <beastly.hpp>
#include <catch2/catch.hpp>

TEST_CASE("Google on root domain", "[follow_redirect]") {
    auto r = beastly::get("https://google.com");
    REQUIRE(r.result() == beastly::status::ok);
    REQUIRE(r.result_int() == 200);
}
