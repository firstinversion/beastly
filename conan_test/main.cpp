#include <iostream>
#include <beastly.hpp>

int main() {
    auto r = beastly::get("http://localhost:3000/api/users");
    std::cout << "Status: " << r.result_int() << '\n';

    return 0;
}
