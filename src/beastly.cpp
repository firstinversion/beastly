#include <beastly.hpp>

#include <beastly/internal/implementations/shared.hpp>
#include <beastly/internal/mozilla_trust_chain.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <string_view>

namespace beast = boost::beast;
namespace http  = beast::http;
namespace net   = boost::asio;
namespace ssl   = net::ssl;
using tcp       = net::ip::tcp;

namespace beastly::internal {

    template <typename Stream>
    response<string_body> write(Stream& stream, const request<string_body>& req) {
        beast::error_code ec;

        http::write(stream, req);

        beast::flat_buffer    buffer;
        response<string_body> res;
        http::read(stream, buffer, res);

        if (ec && ec != beast::errc::not_connected && ec != net::error::eof && !is_ssl_short_read(ec))
            throw beast::system_error{ec};
        return res;
    }

    response<string_body> perform_request(const uri& uri, const request<string_body>& req) {
        beast::error_code ec;

        net::io_context ioc;
        tcp::resolver   resolver{ioc};
        auto const      resolver_results = resolver.resolve(uri.host(), use_port_with_default(uri));

        if (uri.protocol() == beastly::protocol::http) {
            beast::tcp_stream stream{ioc};
            stream.connect(resolver_results);

            return write(stream, req);
        } else {
            ssl::context ctx(ssl::context::tlsv12_client);
            ctx.add_certificate_authority(boost::asio::buffer(beastly::internal::mozilla_trust_chain.c_str(),
                                                              beastly::internal::mozilla_trust_chain.size()),
                                          ec);
            ctx.set_verify_mode(ssl::verify_peer);

            beast::ssl_stream<beast::tcp_stream> stream(ioc, ctx);
            if (!SSL_set_tlsext_host_name(stream.native_handle(), uri.host().c_str())) {
                beast::error_code ec{static_cast<int>(::ERR_get_error()), net::error::get_ssl_category()};
                throw beast::system_error{ec};
            }

            beast::get_lowest_layer(stream).connect(resolver_results);
            stream.handshake(ssl::stream_base::client);

            return write(stream, req);
        }
    }

    response<string_body> following_perform_request(const uri& uri, request<string_body>& req, uint8_t limit) {
        auto res = perform_request(uri, req);
        if (res.result() == status::moved_permanently && limit > 0) {
            auto itr = res.find(http::field::location);
            if (itr == res.end()) throw std::runtime_error("got 301 moved_permanently without a provided location");

            boost::string_view sv       = itr->value();
            auto               next_uri = beastly::uri(std::string_view(sv.begin(), sv.size()));
            req.target(next_uri.target());
            req.set(field::host, next_uri.host());
            return following_perform_request(next_uri, req, limit - 1);
        } else {
            return res;
        }
    }

}  // namespace beastly::internal
