from conans import ConanFile, CMake, tools


class BeastlyConan(ConanFile):
    name = "beastly"
    version = "0.2.3"
    license = "MIT"
    author = "Andrew Rademacher <andrew@firstinversion.io>"
    url = "https://bitbucket.org/firstinversion/beastly"
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto"
    }
    description = "Concise HTTP request library build on Beast."
    topics = ("HTTP", "network", "REST")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = ("boost/1.70.0@conan/stable",
                "fmt/5.3.0@bincrafters/stable")
    build_requires = ("Catch2/2.7.1@catchorg/stable",
                      "jsonformoderncpp/3.6.1@vthiery/stable")
    export_sources = "*"
    no_copy_source = True

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake.build()
            cmake.test()

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*beastly.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["beastly"]
